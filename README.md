![stability-workinprogress](https://bitbucket.org/repo/ekyaeEE/images/477405737-stability_work_in_progress.png)
![internaluse-green](https://bitbucket.org/repo/ekyaeEE/images/3847436881-internal_use_stable.png)
![issues-open](https://bitbucket.org/repo/ekyaeEE/images/2944199103-issues_open.png)

# RATIONALE #

* A mobile app for our meetings and for use of the community. A kind of _checklist_, _guide_ to the auditorium and general conditions 
* This repo is a living document that will grow and adapt over time

### What is this repository for? ###

* Quick summary
    - A mobile app for our meetings
* Version 1.00

### How do I get set up? ###

* Summary of set up
    - _In the making_
* Configuration
    - _In the making_
* Dependencies
    - NPM
	- React Native
* Deployment instructions
    - Since it will be a mobile app (think Android platform), it is a closed-solution. So, there is no deployment instruction as possible.

### Related repositories ###
* [Streaming](https://bitbucket.org/imhicihu/streaming/src)
* [Presentations, norms, checklist](https://bitbucket.org/imhicihu/presentations-norms-checklist-proxies/src/master/)
* [Proxy access](https://bitbucket.org/imhicihu/proxy-access/src/)
* [Setting up Github under proxy](https://bitbucket.org/imhicihu/setting-up-github-under-proxy/src/master/)

### Source ###

* Check them on [here](https://bitbucket.org/imhicihu/conference/src)

### Issues ###

* Check them on [here](https://bitbucket.org/imhicihu/conference/issues)

### Changelog ###

* Please check the [Commits](https://bitbucket.org/imhicihu/conference/commits/) section for the current status

### Who do I talk to? ###

* Repo owner or admin
    - Contact `imhicihu` at `gmail` dot `com`
* Other community or team contact
    - Contact is _enable_ on the [board](https://bitbucket.org/imhicihu/conference/addon/trello/trello-board) of this repo. (You need a [Trello](https://trello.com/) account)

### Code of Conduct

* Please, check our [Code of Conduct](https://bitbucket.org/imhicihu/conference/src/master/code_of_conduct.md)

### Legal ###

* All trademarks are the property of their respective owners.

### License ###

* The content of this project itself is licensed under the ![MIT Licence](https://bitbucket.org/repo/ekyaeEE/images/2049852260-MIT-license-green.png)