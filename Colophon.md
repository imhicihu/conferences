## Software
* Data validator
    - [Goodtables](http://try.goodtables.io/): checks tabular data sources for structural problems
* Image treatment
    - [Image Shrinke](https://github.com/stefansl/image-shrinker): minify images and graphics
* Time zone
    - [Time Zone Converter](https://timezoneconverterapp.com/#download)
* GIT workflow
    - [SourceTree](https://www.sourcetreeapp.com/): GIT client
* Presentation (online)
    - [Speaker Deck](https://speakerdeck.com/): share presentations online. Simply upload your slides as a PDF, and we’ll turn them into a beautiful online experience.
* WIFI signal scanning
    - [FIND3 - WiFi+Bluetooth based local GPS](https://play.google.com/store/apps/details?id=com.internalpositioning.find3.find3app): based on this open source project: https://github.com/schollz/find3-android-scanner
* Note taking / Text edfitor (offline mostly)
    * Android
        - [Evernote](https://play.google.com/store/apps/details?id=com.evernote&hl=es_419)
    * iPad/iPhone
        - [Paper](https://apps.apple.com/es/app/paper/id506003812) compatible with iphone/ipad
        - [Evernote](https://apps.apple.com/ar/app/evernote/id281796108)
    * MacOSX
        - [Brackets](http://brackets.io/)
        - [Atom](https://atom.io/)
    * Windows
        - [Notepad++](https://notepad-plus-plus.org/)
        - [Brackets](http://brackets.io/)
        
## Online tools
* User suggestions
        - [Imhicihu's Uservoice](https://imhicihu.uservoice.com/)